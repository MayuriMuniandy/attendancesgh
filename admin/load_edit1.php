<?php
	require_once 'connect.php';
	$q_edit_staff = $conn->query("SELECT * FROM `staff` WHERE `staff_id` = '$_REQUEST[staff_id]'") or die(mysqli_error());
	$f_edit_staff = $q_edit_staff->fetch_array();
?>
<form method = "POST" action = "edit_staff_query.php?staff_id=<?php echo $f_edit_staff['staff_id']?>" enctype = "multipart/form-data">
	<div class  = "modal-body">
		<div class = "form-group">
			<label>Student ID:</label>
			<input type = "text" name = "staff_no" value = "<?php echo $f_edit_staff['staff_no']?>" required = "required" class = "form-control" />
		</div>
		<div class = "form-group">
			<label>Firstname:</label>
			<input type = "text" name = "firstname" value = "<?php echo $f_edit_staff['firstname']?>" required = "required" class = "form-control" />
		</div>
		<div class = "form-group">
			<label>Middlename:</label>
			<input type = "text" name = "middlename" value = "<?php echo $f_edit_staff['middlename']?>" placeholder = "(Optional)" class = "form-control" />
		</div>
		<div class = "form-group">
			<label>Lastname:</label>
			<input type = "text" name = "lastname" value = "<?php echo htmlentities($f_edit_staff['lastname'])?>" required = "required" class = "form-control" />
		</div>
		<div class = "form-group">
			<label>Activity:</label>
			<input type = "text" value = "<?php echo $f_edit_staff['activity']?>" name = "activity" required = "required" class = "form-control" />
		</div>
		<div class = "form-group">
			<label>Place:</label>
			<input type = "text" name = "section" required = "required" value = "<?php echo $f_edit_staff['place']?>" class = "form-control" />
		</div>
	</div>
	<div class = "modal-footer">
		<button  class = "btn btn-warning"  name = "edit_admin"><span class = "glyphicon glyphicon-edit"></span> Save Changes</button>
	</div>
</form>	